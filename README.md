# Search By Triplet - OpenCL

Welcome to SBT, a FPGA project providing several OpenCL implementations of the VELO tracks reconstruction.

## Getting Started

The project requires an Intel FPGA accelerator supporting OpenCL with Quartus. However, there is a also a CPU version of the code available in the cpu branch of the repository.

You can check your software version as follow :

```
$ aocl version
aocl 16.1.2.203 (Intel(R) FPGA SDK for OpenCL(TM), Version 16.1.2 Build 203, Copyright (C) 2017 Intel Corporation)

$ aoc --version
Intel(R) FPGA SDK for OpenCL(TM), 64-Bit Offline Compiler
Version 16.1.2 Build 203
Copyright (C) 2017 Intel Corporation
```

Some machines with embedded FPGAs are available at CERN and LHCb.

### Techlab machine with Nallatech 385A

A Nallatech FPGA accelerator is available in the Techlab. The only compatible version of Quartus is 16.1.2.

In order to configure the machine with Quartus 16.1.2, add these commands in your .bashrc.
```
source /opt/env_16_1_pro.sh
export INTELFPGAOCLSDKROOT=$ALTERAOCLSDKROOT
```

### Intel PAC Card

The Intel PAC is plugged in the lab42.

In order to configure the machine with Quartus 17.1, add these commands in your .bashrc.

```
# Configure Quartus 17.1 with FPGA SDK for OpenCL
source /scratch/thloisea/inteldevstack/init_env.sh
source /scratch/thloisea/inteldevstack/intelFPGA_pro/hld/init_opencl.sh
export ALTERAOCLSDKROOT=$INTELFPGAOCLSDKROOT
```

## Switching project

The repository is composed of 5 branches

```
$ git branch

  cpu
  fpga
  fpga_ndr
* master
  pipeline
```

To switch the current branch, follow the steps below.

### Switching to FPGA project - Single work-item

```
$ git checkout fpga
```

### Switching to FPGA project - NDRange kernel

```
$ git checkout fpga_ndr
```

### Switching to FPGA project - Pipelined kernel executions

```
$ git checkout pipeline
```

### Switching to CPU Project

```
$ git checkout cpu
```